import React from 'react';
import PropTypes from 'prop-types';
import TableRow from "./TableRow.js";

const TableBody = props => {
  return (
    <tbody>
      {props.users.map(function(it){
        return <TableRow key={it.id} data={it} onRowDeleted={props.onRowDeleted} />
      })}
    </tbody>
  );
}

TableBody.propTypes = {
    users: PropTypes.arrayOf(PropTypes.shape({ id: PropTypes.number, name: PropTypes.string }))
}

export default TableBody;