package eu.halaj.moropriklad.controller;

import eu.halaj.moropriklad.dto.CreateUserDto;
import eu.halaj.moropriklad.dto.UserDto;
import eu.halaj.moropriklad.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
public class UserController {
    private static final String USERS_PATH = "/users";
    private static final String USER_PATH = "/users/{id}";

    private IUserService userService;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping(USERS_PATH)
    List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping(USER_PATH)
    UserDto getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    @PostMapping(USERS_PATH)
    UserDto createUser(@Valid @RequestBody CreateUserDto userDto) {
        return userService.createUser(userDto);
    }

    @PutMapping(USER_PATH)
    UserDto updateUser(@RequestBody UserDto userDto, @PathVariable Long id) {
        return userService.updateUser(userDto);
    }

    @DeleteMapping(USER_PATH)
    void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }
}
