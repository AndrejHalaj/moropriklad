package eu.halaj.moropriklad.service;

import eu.halaj.moropriklad.domain.User;
import eu.halaj.moropriklad.dto.CreateUserDto;
import eu.halaj.moropriklad.dto.UserDto;
import eu.halaj.moropriklad.mapper.UserMapper;
import eu.halaj.moropriklad.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService implements IUserService {
    private UserRepository repository;
    private UserMapper mapper;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository repository, UserMapper mapper, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<UserDto> getAllUsers() {
        return StreamSupport.stream(repository.findAll().spliterator(), false)
                .map(mapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto getUserById(Long id) {
        return mapper.mapToDto(repository.getUserById(id));
    }

    @Override
    public UserDto createUser(CreateUserDto userDto) {
        User user = mapper.mapFromDto(userDto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return mapper.mapToDto(repository.save(user));
    }

    @Override
    public UserDto updateUser(UserDto userDto) {
        return mapper.mapToDto(repository.save(mapper.mapFromDto(userDto)));
    }

    @Override
    public void deleteUser(Long id) {
        repository.deleteById(id);
    }
}
