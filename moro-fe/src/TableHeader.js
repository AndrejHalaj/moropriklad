import React, { Component } from 'react';

class TableHeader extends Component {
    render() {
      return (
        <thead>
          <tr>
            <th>id</th>
            <th>name</th>
          </tr>
        </thead>
      );
    }
}

export default TableHeader;