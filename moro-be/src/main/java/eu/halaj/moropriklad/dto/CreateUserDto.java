package eu.halaj.moropriklad.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateUserDto {
    @NotNull
    @Size(min=6, max=50)
    private String name;

    @NotNull
    @Size(min=6, max=20)
    private String username;

    @NotNull
    @Size(min=8)
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
