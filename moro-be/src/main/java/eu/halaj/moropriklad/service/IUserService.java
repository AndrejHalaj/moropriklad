package eu.halaj.moropriklad.service;

import eu.halaj.moropriklad.dto.CreateUserDto;
import eu.halaj.moropriklad.dto.UserDto;

import java.util.List;

public interface IUserService {
    List<UserDto> getAllUsers();
    UserDto getUserById(Long id);
    UserDto createUser(CreateUserDto user);
    UserDto updateUser(UserDto user);
    void deleteUser(Long id);
}
