import React from 'react';
import PropTypes from 'prop-types';

const TextField = props => {
  return (
    <input type="text" onChange={props.onInputChanged} />
  );
}

TextField.propTypes = {
  onInputChanged: PropTypes.func
}

export default TextField