import React from 'react';
import PropTypes from 'prop-types';
import TableCell from "./TableCell.js";
import Button from "./Button.js";

const TableRow = props => {
  return (
    <tr>
      <TableCell data={props.data.id} />
      <TableCell data={props.data.name} />
      <TableCell data={
        <span>
          <Button text="Smazat" onClick={() => props.onRowDeleted(props.data.id)} />
          <Button text="Upravit" onClick={() => props.onRowDeleted(props.data.id)} />
        </span>
      } />
    </tr>
  );
}

TableRow.propTypes = {
    data: PropTypes.shape({ id: PropTypes.number, name: PropTypes.string })
}

export default TableRow;