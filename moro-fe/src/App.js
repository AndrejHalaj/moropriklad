import React, { Component } from 'react';
import Table from "./Table.js";

class App extends Component {
  constructor() {
    super();
    this.users = [
      {id: 1, name: "Jozko Mrkvicka"},
      {id: 2, name: "Igor Matovic"}
    ]
  }

  render() {
    return (
      <div>
        <Table users={this.users} />
      </div>
    );
  }
}

export default App;
