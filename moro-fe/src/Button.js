import React from 'react';
import PropTypes from 'prop-types';

const Button = props => {
    return (<button type="button" onClick={props.onClick}>{props.text}</button>);
}

Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func
}

export default Button