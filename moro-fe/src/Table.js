import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TableHeader from "./TableHeader.js";
import TableBody from "./TableBody.js";
import TextField from "./TextField.js";
import Button from "./Button.js";

class Table extends Component {
  constructor(props) {
    super(props)
    this.state = { users: props.users, currentInput: "" }

    this.onRowDeleted = this.onRowDeleted.bind(this)
    this.handleInputChanged = this.handleInputChanged.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  onRowDeleted(id) {
    const newState = this.state
    const index = newState.users.findIndex(a => a.id === id);

    if (index === -1) return;
    newState.users.splice(index, 1);

    this.setState(newState);
  }

  handleInputChanged(event) {
    const newState = this.state
    newState.currentInput = event.target.value
    this.setState(newState)
  }

  handleSubmit() {
    const newState = this.state
    newState.users.push({ id: 7, name: this.state.currentInput })
    this.setState(newState)
  }

  render() {
    return (
    <div>
      <table>
        <TableHeader />
        <TableBody users={this.state.users} onRowDeleted={this.onRowDeleted} />
      </table>
      <TextField onInputChanged={this.handleInputChanged} />
      <Button text="Přidat" onClick={this.handleSubmit} />
    </div>
    );
  }
}

Table.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape({ id: PropTypes.number, name: PropTypes.string }))
}

export default Table;